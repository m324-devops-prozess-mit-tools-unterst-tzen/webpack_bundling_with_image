import '../styles/main.css';
import { init as initCountries } from './countries.js';
import { init as initColorSelection } from './colorSelection.js';
import { init as initCounter } from './counter.js';

initCountries();
initCounter();
initColorSelection();
